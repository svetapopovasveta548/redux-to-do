import React from "react";
import Card from "../UI/Card";
import styles from "./InputToDo.module.css";
import { useState } from "react";
import { ToDoInput } from "../../redux/actions/inputInfo"
import { useDispatch } from "react-redux";


function InputToDo() {
  const dispatch = useDispatch();


  const [inputValue, setInputValue] = useState("");
  const [isDisabled, setIsDisabled] = useState(false);

  const getdInputValue = (event) => {
    setIsDisabled(event.target.value.length > 0 );
    setInputValue(event.target.value);
  };


  const sendInputValue = (event) => {
    event.preventDefault();
    
    const ToDoItem = {
      id: Math.random().toString(),
      task: inputValue,
    };

    dispatch(ToDoInput(ToDoItem))
    setInputValue("");
    setIsDisabled(!isDisabled)
  };

  return (
    <Card>
      <form onSubmit={sendInputValue}  className={styles.form}>
        <input value={inputValue} onChange={getdInputValue} type="text" />
        <button disabled={!isDisabled} type="submit">
          Create ToDo
        </button>
      </form>
    </Card>
  );
}

export default InputToDo;
