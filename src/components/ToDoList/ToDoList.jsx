import ToDoCard from "../ToDoCard/ToDoCard";
import { useSelector } from "react-redux"


function ToDoList(props) {
  const arrayOfToDoItems = useSelector(state => state.input.arrCards)


  return <>{arrayOfToDoItems.map(todo => (<ToDoCard deleteToDoCard={props.deleteToDoCard} key={todo.id} todo={todo}/>))}</>;
}

export default ToDoList;
