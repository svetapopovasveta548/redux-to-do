import InputToDo from "../InputToDo/InputToDo";
import React from "react";
import ToDoList from "../ToDoList/ToDoList";

function Home() {
  return (
    <React.Fragment>
      <InputToDo />
      <ToDoList />
    </React.Fragment>
  );
}

export default Home;
