import React from "react";
import { useState } from "react";
import Card from "../UI/Card";
import styles from "./ToDoCard.module.css";
import checkboxUndone from "./checkbox-unchecked-svgrepo-com.svg";
import checkboxDone from "./checkbox-checked-svgrepo-com.svg";
import cross from "./cross.svg";
import { deleteCard } from "../../redux/actions/inputInfo";
import { useDispatch } from "react-redux";

function ToDoCard(props) {
  const dispatch = useDispatch();

  const [isDone, setIsDone] = useState(false);

  const { id, task } = props.todo;
  const [] = useState(true);

  const toggle = () => {
    setIsDone(!isDone);
  };

  const getIdForDelete = () => {
    dispatch(deleteCard(id));
  };
  return (
    <Card>
      <div className={styles.wrapper}>
        <img
          onClick={getIdForDelete}
          className={styles.cross}
          src={cross}
          alt=""
        />
        <p className={`${isDone ? styles.task : ""}`}>{task}</p>
        <div onClick={toggle}>
          <img
            className={styles.checkboxUndone}
            src={isDone ? checkboxDone : checkboxUndone}
            alt=""
          />
        </div>
      </div>
    </Card>
  );
}

export default ToDoCard;
// {`${itemBack ? "active" : ""}`
