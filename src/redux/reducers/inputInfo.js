import { toDoTypes } from "../types";

const initialState = {
  arrCards: [],
};

export function inputReducer(state = initialState, action) {
  switch (action.type) {
    case toDoTypes.ADD_NEW_TODO_CARD:
      return {
        ...state,
        arrCards: [...state.arrCards, action.payload.card],
      };
    case toDoTypes.REMOVE_TODO_CARD:
      const filteredCards = state.arrCards.filter(
        (card) => card.id !== action.payload.id
      );

      return {
        ...state,
        arrCards: filteredCards,
      };
    default:
      return state;
  }
}

