import { toDoTypes } from "../types";

export function ToDoInput(card) {
    return {
      type: toDoTypes.ADD_NEW_TODO_CARD,
      payload: {
        card,
      },
  
    };
  }

  export function deleteCard(id) {
    return {
      type: toDoTypes.REMOVE_TODO_CARD,
      payload: {
        id,
      },
    };
  }
  
  