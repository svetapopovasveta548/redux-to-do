import { combineReducers } from "redux";
// import { cardsReducer as cards } from "./reducers/cards";
import { inputReducer as input } from "./reducers/inputInfo";

export const rootReducer = combineReducers({ input });
